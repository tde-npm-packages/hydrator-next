import { DataDirection } from './DataDirection';

export interface DataConverterInfo {
	/**
	 * Type of field, converters has to be registered in order for this field to even work
	 */
	type?: string;

	/**
	 * Nested class object type or `forwardRef` of that class object
	 */
	nested?: any;

	/**
	 * Used with conjunction with nested, populates a key of child with current object
	 *
	 * **WARNING** This creates circular references
	 */
	parentKey?: string;

	/**
	 * Indicate that field is an array
	 */
	array?: boolean;

	/**
	 * Forces field to remain array no matter input data
	 */
	forceArray?: boolean;

	/**
	 * Custom converter function
	 *
	 * @param value - value passed to converter
	 * @param direction - data direction
	 * @param type - `string` passed as type of field
	 */
	converter?: (value: any, direction: DataDirection, type?: string) => any;

	/**
	 * translates key when converting object to plain in any direction
	 */
	translate?: string;

	/**
	 * translates key when converting object to plain and direction to plain object
	 */
	translatePlain?: string;

	/**
	 * translates key when converting object to plain and direction to database
	 */
	translateDatabase?: string;

	/**
	 * Value will be ignored when coming or going to/from database
	 *
	 * * `true` = ignore completely
	 * * `'hydrate'` - when hydrating object with plain data, field will be ignored
	 * * `'dehydrate'` - when dehydrating class instance value will not be preserved in plain object
	 */
	ignoreDatabase?: boolean | 'dehydrate' | 'hydrate';

	/**
	 * Value will be ignored when coming or going to plain object
	 *
	 * * `true` = ignore completely
	 * * `'hydrate'` - when hydrating object with plain data, field will be ignored
	 * * `'dehydrate'` - when dehydrating class instance value will not be preserved in plain object
	 */
	ignorePlain?: boolean | 'dehydrate' | 'hydrate';

	/**
	 * Value will be ignored when coming or going to database or plain object
	 *
	 * * `true` = ignore completely
	 * * `'hydrate'` - when hydrating object with plain data, field will be ignored
	 * * `'dehydrate'` - when dehydrating class instance value will not be preserved in plain object
	 */
	ignore?: boolean | 'dehydrate' | 'hydrate';
}