[hydrator-next](../README.md) / [Exports](../modules.md) / Converter

# Class: Converter

## Table of contents

### Constructors

- [constructor](converter.md#constructor)

### Properties

- [converters](converter.md#converters)

### Methods

- [convert](converter.md#convert)
- [convertSingle](converter.md#convertsingle)
- [register](converter.md#register)
- [registerConverter](converter.md#registerconverter)

## Constructors

### constructor

• **new Converter**()

## Properties

### converters

▪ `Static` **converters**: `Map`<`string`, [`ConverterFunction`](../modules.md#converterfunction)\>

## Methods

### convert

▸ `Static` **convert**(`value`, `info`, `direction`): `any`

Converts a value from one type to another including arrays.

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |
| `info` | [`DataConverterInfo`](../interfaces/dataconverterinfo.md) |
| `direction` | [`DataDirection`](../enums/datadirection.md) |

#### Returns

`any`

___

### convertSingle

▸ `Static` **convertSingle**(`value`, `info`, `direction`): `any`

Converts a single value from one type to another.

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |
| `info` | [`DataConverterInfo`](../interfaces/dataconverterinfo.md) |
| `direction` | [`DataDirection`](../enums/datadirection.md) |

#### Returns

`any`

___

### register

▸ `Static` **register**(`type`, `converter`): `void`

Registers a converter for a specific type. Does nothing if the converter is already registered.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `type` | `string` | The type to register the converter for. |
| `converter` | [`ConverterFunction`](../modules.md#converterfunction) | The converter to register. |

#### Returns

`void`

___

### registerConverter

▸ `Static` **registerConverter**(`type`, `converter`): `void`

Registers a converter for a specific type. Throws an error if the converter is already registered.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `type` | `string` | The type to register the converter for. |
| `converter` | [`ConverterFunction`](../modules.md#converterfunction) | The converter to register. |

#### Returns

`void`
