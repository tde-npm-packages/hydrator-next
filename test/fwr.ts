import { FieldType, forwardRef, Hydrator } from '../src';

class Car {
	name: string = '';
	@FieldType({ nested: forwardRef(() => CarPrototype), parentKey: 'cars' })
	prototype: CarPrototype;
}

class CarPrototype {
	name: string = '';

	@FieldType({ nested: forwardRef(() => Car), array: true, parentKey: 'prototype' })
	cars: Car[];
}

const data = {
	name: 'Volvo S80',
	prototype: {
		name: 'Volvo Sedan'
	}
};

const car = Hydrator.hydrate(Car, data);
console.log(car);

const data2 = {
	name: 'Volvo Sedan',
	cars: [
		{ name: 'Volvo S40' },
		{ name: 'Volvo S60' },
		{ name: 'Volvo S80' }
	]
};
const pr = Hydrator.hydrate(CarPrototype, data2);
console.log(pr);

Hydrator.MaxNesting = 2;
const dehpr = Hydrator.dehydrate(pr);
console.log(JSON.stringify(dehpr, null, 4));