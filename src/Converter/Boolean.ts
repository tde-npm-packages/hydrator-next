export function convertBoolean(value: any) {
	return value === null ? null : !!value;
}