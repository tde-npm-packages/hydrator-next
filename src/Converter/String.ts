export function convertString(value: any) {
	return value === null ? null : (value.toString ? value.toString() : null);
}