import { Converter }         from './Converter';
import { DataConverterInfo } from './DataConverterInfo';
import { DataDirection }     from './DataDirection';
import 'reflect-metadata';

function upperCaseFirst(s: string) {
	return s.substr(0, 1).toUpperCase() + s.substr(1);
}

/**
 *
 */
export class Hydrator {
	/**
	 * Maximum level of nesting for dehydration, useful for objects with circular references
	 */
	public static MaxNesting = 10;
	/**
	 * Should we warn about exceeding nesting level?
	 */
	public static WarnNesting = false;

	/**
	 * Sets a single property to entity, respects setters ex 'someProp'
	 * will try to check if function `setSomeProp` exists
	 *
	 * @param entity class object
	 * @param prop property name
	 * @param value property value
	 */
	static setProperty(entity: any, prop: string, value: any) {
		const setter = 'set' + upperCaseFirst(prop);
		if (entity[setter]) {
			entity[setter](value);
		}
		else {
			entity[prop] = value;
		}

		return entity;
	}

	/**
	 * Gets a single property from entity, respects getters ex 'someprop'
	 * will try to check if function `getSomeProp` exists
	 *
	 * @param entity class object
	 * @param prop property name
	 */
	static getProperty(entity: any, prop: string) {
		const getter = 'get' + upperCaseFirst(prop);
		if (entity[getter]) {
			return entity[getter]();
		}
		else {
			return entity[prop];
		}
	}

	/**
	 * Gets a class properties, those properties either had to be intialized with a value or decorated
	 * with {@link FieldType}
	 *
	 * @param entity object of some class
	 */
	static getClassProperties(entity: any) {
		const fieldTypes = Reflect.getMetadata('FieldType', entity);
		const keys = Object.keys(entity);

		for (const customKey in fieldTypes) {
			if (!Object.prototype.hasOwnProperty.call(fieldTypes, customKey)) {
				continue;
			}

			if (keys.indexOf(customKey) > -1) {
				continue;
			}

			keys.push(customKey);
		}

		return keys;
	}

	/**
	 * Converts entity class to plain object, if toDatabase parameter is true, special handling of id fields
	 * will be applied
	 *
	 * @param entity object of some class
	 * @param toDatabase should that be dehydrated to be used for database insert/update
	 * @param ignoreClassFunction should we ignore function `dehydrate` if class has it
	 * @param currentLevel - **INTERNAL**
	 */
	static dehydrate<T>(
		entity: T,
		toDatabase = false,
		ignoreClassFunction = false,
		currentLevel = 0
	): any {
		if (entity === null) {
			return entity;
		}
		if (entity === undefined) {
			return undefined;
		}
		if (!ignoreClassFunction && typeof (entity as any).dehydrate === 'function') {
			return (entity as any).dehydrate(toDatabase, currentLevel);
		}

		currentLevel++;
		if (currentLevel > Hydrator.MaxNesting + 1) {
			if (Hydrator.WarnNesting) {
				console.log(`Maximum level of nesting ${ Hydrator.MaxNesting } reached`);
			}
			return;
		}

		const result: any = {};
		const fieldTypes = Reflect.getMetadata('FieldType', entity);
		const keys = Hydrator.getClassProperties(entity);

		for (const k of keys) {
			let value: any = Hydrator.getProperty(entity, k);
			let key = k;

			if (fieldTypes && fieldTypes[k]) {
				const fieldType: DataConverterInfo = fieldTypes[k];

				if (fieldType.translate) {
					key = fieldType.translate;
				}

				if (fieldType.translatePlain && !toDatabase) {
					key = fieldType.translatePlain;
				}

				if (fieldType.translateDatabase && toDatabase) {
					key = fieldType.translateDatabase;
				}

				if (fieldType.ignore === true || fieldType.ignore === 'dehydrate') {
					continue;
				}

				if (toDatabase) {
					if (fieldType.ignoreDatabase === true || fieldType.ignoreDatabase === 'dehydrate') {
						continue;
					}
				}
				else {
					if (fieldType.ignorePlain === true || fieldType.ignorePlain === 'dehydrate') {
						continue;
					}
				}

				if (fieldType.nested) {
					if (typeof value === 'undefined') {
						continue;
					}

					if (fieldType.array) {
						value = Hydrator.dehydrateArray(value, toDatabase, false, currentLevel);
					}
					else {
						value = Hydrator.dehydrate(value, toDatabase, false, currentLevel);
					}
				}
				else {
					const dataDirection = toDatabase ? DataDirection.ToDatabase : DataDirection.ToPlain;
					value = Converter.convert(value, fieldType, dataDirection);
				}
			}

			result[key] = value;
		}

		return result;
	}

	// noinspection JSUnusedGlobalSymbols
	/**
	 * Converts array of classes back to array of plain objects
	 *
	 * @param entities object of some class
	 * @param toDatabase should that be dehydrated to be used for database insert/update
	 * @param ignoreClassFunction should we ignore function `dehydrate` if class has it
	 * @param currentLevel **INTERNAL**
	 */
	static dehydrateArray<T>(
		entities: T[],
		toDatabase = false,
		ignoreClassFunction = false,
		currentLevel = 0
	): any[] | any {
		currentLevel++;
		if (currentLevel > Hydrator.MaxNesting + 1) {
			if (Hydrator.WarnNesting) {
				console.log(`Maximum level of nesting ${ Hydrator.MaxNesting } reached`);
			}

			return [];
		}

		if (entities === null) {
			return null;
		}

		const out = [];
		for (const entity of entities) {
			out.push(Hydrator.dehydrate(entity, toDatabase, ignoreClassFunction, currentLevel));
		}

		return out;
	}

	/**
	 * Converts plain object into it's class representation
	 *
	 * @param entity object of some class
	 * @param data data to insert into new class
	 * @param fromDatabase does the data comes from database
	 * @param ignoreClassFunction should we ignore function `dehydrate` if class has it
	 * @param currentLevel **INTERNAL**
	 */
	static hydrate<T>(
		entity: T | (new() => T),
		data: any,
		fromDatabase = false,
		ignoreClassFunction = false,
		currentLevel = 0
	): T | null | undefined {
		if (data === undefined) {
			return undefined;
		}

		if (data === null) {
			return null;
		}

		if (typeof entity === 'function') {
			entity = new (entity as any)() as T;
		}

		if (!ignoreClassFunction && typeof (entity as any).hydrate === 'function') {
			return (entity as any).hydrate(data, fromDatabase) as T;
		}

		currentLevel++;
		if (currentLevel > Hydrator.MaxNesting + 1) {
			if (Hydrator.WarnNesting) {
				console.log(`Maximum level of nesting ${ Hydrator.MaxNesting } reached`);
			}
			return;
		}

		const fieldTypes = Reflect.getMetadata('FieldType', entity);
		const keys = Hydrator.getClassProperties(entity);

		for (const k of keys) {
			let key = k;
			let value;

			if (fieldTypes && fieldTypes[k]) {
				const fieldType: DataConverterInfo = fieldTypes[k];

				if (fieldType.translate) {
					key = fieldType.translate;
				}

				if (fieldType.translatePlain && !fromDatabase) {
					key = fieldType.translatePlain;
				}

				if (fieldType.translateDatabase && fromDatabase) {
					key = fieldType.translateDatabase;
				}

				if (fieldType.ignore === true || fieldType.ignore === 'hydrate') {
					continue;
				}

				if (fromDatabase) {
					if (fieldType.ignoreDatabase === true || fieldType.ignoreDatabase === 'hydrate') {
						continue;
					}
				}
				else {
					if (fieldType.ignorePlain === true || fieldType.ignorePlain === 'hydrate') {
						continue;
					}
				}

				if (fieldType.nested) {
					const nestedType = fieldType.nested.prototype ? fieldType.nested : fieldType.nested();
					if (data[key] === null || data[key] === undefined) {
						if (fieldType.forceArray) {
							value = [];
						} else {
							value = data[key];
						}
					}
					else {
						if (fieldType.array) {
							value = Hydrator.hydrateArray(nestedType, data[key], fromDatabase);
							if (value && fieldType.parentKey && value.length > 0) {
								const childFieldTypes = Reflect.getMetadata('FieldType', (value as any)[0]);
								const childFieldType: DataConverterInfo = childFieldTypes[fieldType.parentKey];

								for (const child of value) {
									(child as any)[fieldType.parentKey] = childFieldType.array ? [entity] : entity;
								}
							}

							if (fieldType.forceArray && !Array.isArray(value)) {
								value = [];
							}
						}
						else {
							value = Hydrator.hydrate(nestedType, data[key], fromDatabase);
							if (fieldType.parentKey) {
								const childFieldTypes = Reflect.getMetadata('FieldType', value);
								const childFieldType: DataConverterInfo = childFieldTypes[fieldType.parentKey];

								value[fieldType.parentKey] = childFieldType.array ? [entity] : entity;
							}
						}
					}
				}
				else {
					value = data[key];

					const dataDirection = fromDatabase ? DataDirection.FromDatabase : DataDirection.FromPlain;
					value = Converter.convert(value, fieldType, dataDirection);
				}
			}
			else {
				value = data[key];
			}

			// do not set undefined variables on partial data
			if (typeof value === 'undefined') {
				continue;
			}

			Hydrator.setProperty(entity, k, value);
		}

		return entity as T;
	}

	// noinspection JSUnusedGlobalSymbols
	/**
	 * Converts array of plain objects into array of hydrated classes
	 *
	 * @param entityClass object of some class
	 * @param data array of data to pass
	 * @param fromDatabase does this data come from database
	 * @param ignoreClassFunction should we ignore function `hydrate` if class has it
	 * @param currentLevel **INTERNAL**
	 */
	static hydrateArray<T>(
		entityClass: new () => T,
		data: undefined | null | any[],
		fromDatabase = false,
		ignoreClassFunction = false,
		currentLevel = 0
	): null | undefined | (T | null | undefined)[] {
		if (data === null) {
			return null;
		}

		if (data === undefined) {
			return undefined;
		}

		currentLevel++;
		if (currentLevel > Hydrator.MaxNesting + 1) {
			if (Hydrator.WarnNesting) {
				console.log(`Maximum level of nesting ${ Hydrator.MaxNesting } reached`);
			}

			return [];
		}

		const result: (T | null | undefined)[] = [];
		for (const row of data) {
			result.push(Hydrator.hydrate<T>(entityClass, row, fromDatabase, ignoreClassFunction));
		}

		return result;
	}
}