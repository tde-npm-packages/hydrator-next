import { DataDirection } from '../DataDirection';

export function convertNumber(value: any, dataDirection: DataDirection) {
	switch (dataDirection) {
		case DataDirection.FromPlain:
		case DataDirection.FromDatabase:
		case DataDirection.ToPlain:
		case DataDirection.ToDatabase:
			return value === null || value === '' || typeof value === 'undefined' ? null : +value;
	}
}