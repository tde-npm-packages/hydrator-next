import { convertBigInt, Converter, convertNumber, convertString, DataDirection, FieldType, Hydrator } from '../src';
import { Test }                                                                                       from './Test';

Converter.registerConverter('string', convertString);
Converter.registerConverter('number', convertNumber);
Converter.registerConverter('bigint', convertBigInt);

function stringify(o: any) {
	return JSON.stringify(o, (key: string, value: any) =>
		typeof value === 'bigint'
		? value.toString()
		: value // return everything else unchanged
	);
}

class TestClass {
	a = 9;
	b = 'asd';
	@FieldType({ type: 'string' })
	str = 'xsd';

	constructor(a?: number, b?: string, str?: string) {
		if (a) {
			this.a = a;
		}
		if (b) {
			this.b = b;
		}
		if (str) {
			this.str = str;
		}
	}
}

class TestClassForced {
	@FieldType({ type: 'string' })
	str?: string;
	@FieldType({ type: 'number' })
	num?: number;
	@FieldType({ type: 'bigint' })
	bi?: BigInt;
}

class TestClassNested {
	@FieldType({ nested: TestClass })
	a: TestClass | null = null;
}

class TestClassNestedArray {
	@FieldType({ nested: TestClass, array: true })
	a: TestClass[] | null = [];
}

const d1 = new TestClass();
(d1 as any).str = 123;

const d2 = new TestClassForced();
(d2 as any).str = 999;
(d2 as any).num = '123321';
(d2 as any).bi = 123123123;

const d3 = new TestClassNested();
d3.a = new TestClass();

const d4 = new TestClassNestedArray();
d4.a!.push(new TestClass(1, 'a', 'b'));
d4.a!.push(new TestClass(2, 'c', 'd'));
d4.a!.push(new TestClass(3, 'e', 'f'));


const d5 = new TestClassNestedArray();
d5.a = null;

const tests = [
	{
		name: 'dehydrate null',
		cl: null,
		expect: `null`
	},
	{
		name: 'dehydrate undefined',
		cl: undefined,
		expect: undefined
	},
	{
		name: 'dehydrate simple',
		cl: new TestClass(),
		expect: `{"a":9,"b":"asd","str":"xsd"}`
	},
	{
		name: 'dehydrate simple 2',
		cl: d1,
		expect: `{"a":9,"b":"asd","str":"123"}`
	},
	{
		name: 'dehydrate simple 3',
		cl: d2,
		expect: `{"str":"999","num":123321,"bi":123123123}`
	},
	{
		name: 'dehydrate nested',
		cl: d3,
		expect: `{"a":{"a":9,"b":"asd","str":"xsd"}}`
	},
	{
		name: 'dehydrate nested null',
		cl: new TestClassNested(),
		expect: `{"a":null}`
	},
	{
		name: 'dehydrate nested array',
		cl: d4,
		expect: `{"a":[{"a":1,"b":"a","str":"b"},{"a":2,"b":"c","str":"d"},{"a":3,"b":"e","str":"f"}]}`
	},
	{
		name: 'dehydrate nested array null',
		cl: d5,
		expect: `{"a":null}`
	}
];

for (const test of tests) {
	Test.run(test.name, () => {
		const out = Hydrator.dehydrate(test.cl as any, (test as any).fromDb);
		console.log(out);
		const string = stringify(out);
		if (string !== test.expect) {
			console.log(string);
			return string;
		}
		return true;
	});
}
