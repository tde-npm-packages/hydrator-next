import { DataConverterInfo } from './DataConverterInfo';
import 'reflect-metadata';

export function FieldType(opts: DataConverterInfo) {
	// Third parameter is required for constructor
	return (target: any, propertyKey: string | symbol, _?: any) => {
		const allMetadata = Reflect.getMetadata('FieldType', target) || {};

		/**
		 * Make copy of metadata
		 * https://github.com/rbuckton/reflect-metadata/issues/53
		 */
		const newMetadata: any = {};
		for (const key of Object.keys(allMetadata)) {
			newMetadata[key] = allMetadata[key];
		}

		/**
		 * Add and register new metadata
		 */
		newMetadata[propertyKey] = opts;
		Reflect.defineMetadata('FieldType', newMetadata, target);
	};
}