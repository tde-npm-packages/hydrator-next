[hydrator-next](../README.md) / [Exports](../modules.md) / Hydrator

# Class: Hydrator

## Table of contents

### Constructors

- [constructor](hydrator.md#constructor)

### Properties

- [MaxNesting](hydrator.md#maxnesting)
- [WarnNesting](hydrator.md#warnnesting)

### Methods

- [dehydrate](hydrator.md#dehydrate)
- [dehydrateArray](hydrator.md#dehydratearray)
- [getClassProperties](hydrator.md#getclassproperties)
- [getProperty](hydrator.md#getproperty)
- [hydrate](hydrator.md#hydrate)
- [hydrateArray](hydrator.md#hydratearray)
- [setProperty](hydrator.md#setproperty)

## Constructors

### constructor

• **new Hydrator**()

## Properties

### MaxNesting

▪ `Static` **MaxNesting**: `number` = `10`

Maximum level of nesting for dehydration, useful for objects with circular references

___

### WarnNesting

▪ `Static` **WarnNesting**: `boolean` = `false`

Should we warn about exceeding nesting level?

## Methods

### dehydrate

▸ `Static` **dehydrate**<`T`\>(`entity`, `toDatabase?`, `ignoreClassFunction?`, `currentLevel?`): `any`

Converts entity class to plain object, if toDatabase parameter is true, special handling of id fields
will be applied

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `entity` | `T` | `undefined` | object of some class |
| `toDatabase` | `boolean` | `false` | should that be dehydrated to be used for database insert/update |
| `ignoreClassFunction` | `boolean` | `false` | should we ignore function `dehydrate` if class has it |
| `currentLevel` | `number` | `0` | **INTERNAL** |

#### Returns

`any`

___

### dehydrateArray

▸ `Static` **dehydrateArray**<`T`\>(`entities`, `toDatabase?`, `ignoreClassFunction?`, `currentLevel?`): `any`[]

Converts array of classes back to array of plain objects

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `entities` | `T`[] | `undefined` | object of some class |
| `toDatabase` | `boolean` | `false` | should that be dehydrated to be used for database insert/update |
| `ignoreClassFunction` | `boolean` | `false` | should we ignore function `dehydrate` if class has it |
| `currentLevel` | `number` | `0` | **INTERNAL** |

#### Returns

`any`[]

___

### getClassProperties

▸ `Static` **getClassProperties**(`entity`): `string`[]

Gets a class properties, those properties either had to be intialized with a value or decorated
with [FieldType](../modules.md#fieldtype)

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `entity` | `any` | object of some class |

#### Returns

`string`[]

___

### getProperty

▸ `Static` **getProperty**(`entity`, `prop`): `any`

Gets a single property from entity, respects getters ex 'someprop'
will try to check if function `getSomeProp` exists

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `entity` | `any` | class object |
| `prop` | `string` | property name |

#### Returns

`any`

___

### hydrate

▸ `Static` **hydrate**<`T`\>(`entity`, `data`, `fromDatabase?`, `ignoreClassFunction?`, `currentLevel?`): `T`

Converts plain object into it's class representation

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `entity` | `T` \| () => `T` | `undefined` | object of some class |
| `data` | `any` | `undefined` | data to insert into new class |
| `fromDatabase` | `boolean` | `false` | does the data comes from database |
| `ignoreClassFunction` | `boolean` | `false` | should we ignore function `dehydrate` if class has it |
| `currentLevel` | `number` | `0` | **INTERNAL** |

#### Returns

`T`

___

### hydrateArray

▸ `Static` **hydrateArray**<`T`\>(`entityClass`, `data`, `fromDatabase?`, `ignoreClassFunction?`, `currentLevel?`): `T`[]

Converts array of plain objects into array of hydrated classes

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `entityClass` | () => `T` | `undefined` | object of some class |
| `data` | `any`[] | `undefined` | array of data to pass |
| `fromDatabase` | `boolean` | `false` | does this data come from database |
| `ignoreClassFunction` | `boolean` | `false` | should we ignore function `hydrate` if class has it |
| `currentLevel` | `number` | `0` | **INTERNAL** |

#### Returns

`T`[]

___

### setProperty

▸ `Static` **setProperty**(`entity`, `prop`, `value`): `any`

Sets a single property to entity, respects setters ex 'someProp'
will try to check if function `setSomeProp` exists

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `entity` | `any` | class object |
| `prop` | `string` | property name |
| `value` | `any` | property value |

#### Returns

`any`
