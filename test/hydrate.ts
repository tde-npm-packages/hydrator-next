import { convertBigInt, Converter, convertNumber, convertString, DataDirection, FieldType, Hydrator } from '../src';
import { Test }                                                                                       from './Test';

Converter.registerConverter('string', convertString);
Converter.registerConverter('number', convertNumber);
Converter.registerConverter('bigint', convertBigInt);

function stringify(o: any) {
	return JSON.stringify(o, (key: string, value: any) =>
		typeof value === 'bigint'
		? value.toString()
		: value // return everything else unchanged
	);
}

class TestClass {
	simple = 1;

	@FieldType({ type: 'string' })
	forcedString = 'aaa';

	@FieldType({ type: 'number' })
	fNumber = 'aaa';

	@FieldType({ type: 'bigint' })
	fbint = 'aaa';

	// should not be in class
	virtual?: number;

	// not initialized but defined should be hydrated
	@FieldType({ type: 'string' })
	ninit: any;
}

const d1 = {
	simple: 2,
	forcedString: 55,
	fNumber: '123',
	fbint: BigInt('1234567812354657678823425'),
	ninit: 'str',
	// this should be ignored
	additional: 'not in class'
};

class TestClassNested {
	@FieldType({ nested: TestClass })
	v: any;
}

class TestClassNested2 {
	@FieldType({ nested: TestClass })
	v: any = null;
}

class TestClassNestedArray {
	@FieldType({ nested: TestClass, array: true })
	v: TestClass[] | null = null;
}

const d2 = {
	v: {
		simple: 9,
		forcedString: 99999,
		fNumber: 'asdsda',
		// this should be ignored
		additional: 'not in class'
	}
};

class TestClassArray {
	@FieldType({ type: 'string', array: true })
	a: any[] | null = null;
}

class TestClassArray2 {
	@FieldType({ array: true })
	a: any[] | null = null;
}

class TestClassArrayNested {
	@FieldType({ array: true, nested: TestClass })
	a: any[] | null = null;
}

class TestClassArrayNestedForced {
	@FieldType({ array: true, nested: TestClass, forceArray: true })
	a: any[] | null = null;
}

class TestClassArrayNestedForcedUndef {
	@FieldType({ array: true, nested: TestClass, forceArray: true })
	a?: any[];
}

const d3 = {
	// undefined is not actual value, gets converted to null
	a: [1, 2, 3, '5', '9', null, undefined]
};

const d3n = {
	// undefined is not actual value, gets converted to null
	a: null
};

class TestClassFromDatabase {
	@FieldType({ type: 'string', translateDatabase: 'otherprop' })
	a?: string;
	@FieldType({ type: 'number', translateDatabase: '_id' })
	id?: string;
}

class TestCtorClass {
	constructor(
		@FieldType({type: 'string'})
		public val: string = ''
	) {}
}

const d4 = {
	otherprop: 'value from db',
	_id: 99
};

function customConverter(value: any, direction: DataDirection, type?: string) {
	switch (direction) {
		case DataDirection.FromDatabase:
		case DataDirection.FromPlain:
			return '_' + value;
		case DataDirection.ToDatabase:
		case DataDirection.ToPlain:
			return value.substr(1);
	}
}

class TestClassCustom {
	@FieldType({ converter: customConverter })
	cust: any;
}

const d5 = {
	cust: 'asd'
};

class TestClassTranslate {
	@FieldType({ type: 'string', translate: 'two' })
	one?: string;
}

const d6 = {
	two: 'value from plain but translated',
	_id: 99
};

// hydrator should not allow this behaviour
class TestClassInjection {
	b = '1';

	hasOwnProperty(...args: any[]) {
		return true;
	}
}

const d7 = {
	a: 1,
	b: 7
};

class TestClassIgnore {
	@FieldType({ ignore: true })
	a = 9;

	b = '1';
}

const d8 = {
	a: 1,
	b: 7
};

const tests = [
	{
		name: 'hydrate ctor',
		cl: TestCtorClass,
		data: {val: 'abc'},
		expect: `{"val":"abc"}`
	},
	{
		name: 'hydrate null',
		cl: TestClass,
		data: null,
		expect: `null`
	},
	{
		name: 'hydrate nested null',
		cl: TestClassNested,
		data: {v: null},
		expect: `{"v":null}`
	},
	{
		name: 'hydrate undefined',
		cl: TestClass,
		data: undefined,
		expect: undefined
	},
	{
		name: 'hydrate nested undefined',
		cl: TestClassNested2,
		data: {v: undefined},
		expect: `{"v":null}`
	},
	{
		name: 'hydrate simple',
		cl: TestClass,
		data: d1,
		expect: `{"simple":2,"forcedString":"55","fNumber":123,"fbint":"1234567812354657678823425","ninit":"str"}`
	},
	{
		name: 'hydrate nested',
		cl: TestClassNested,
		data: d2,
		expect: `{"v":{"simple":9,"forcedString":"99999","fNumber":null,"fbint":"aaa"}}`
	},
	{
		name: 'hydrate nested null',
		cl: TestClassNested,
		data: { v: null },
		expect: `{"v":null}`
	},
	{
		name: 'hydrate nested array',
		cl: TestClassNestedArray,
		data: {
			v: [
				{ simple: 1, forcedString: 'aaa', fNumber: 'aaa' },
				{ simple: 2, forcedString: 'aaa1', fNumber: 'aaa2' },
				{ simple: 3, forcedString: 'aaa3', fNumber: 'aaa4' }
			]
		},
		expect: `{"v":[{"simple":1,"forcedString":"aaa","fNumber":null,"fbint":"aaa"},{"simple":2,"forcedString":"aaa1","fNumber":null,"fbint":"aaa"},{"simple":3,"forcedString":"aaa3","fNumber":null,"fbint":"aaa"}]}`
	},
	{
		name: 'hydrate nested array null',
		cl: TestClassNestedArray,
		data: {
			v: null
		},
		expect: `{"v":null}`
	},
	{
		name: 'hydrate array',
		cl: TestClassArray,
		data: d3,
		expect: `{"a":["1","2","3","5","9",null,null]}`
	},
	{
		name: 'hydrate array null',
		cl: TestClassArray2,
		data: d3n,
		expect: `{"a":null}`
	},
	{
		name: 'hydrate nested array null',
		cl: TestClassArrayNested,
		data: d3n,
		expect: `{"a":null}`
	},
	{
		name: 'hydrate nested array null forced',
		cl: TestClassArrayNestedForced,
		data: {a: null},
		expect: `{"a":[]}`
	},
	{
		name: 'hydrate nested array null forced',
		cl: TestClassArrayNestedForcedUndef,
		data: {a: undefined},
		expect: `{"a":[]}`
	},
	{
		name: 'hydrate nested array undefined',
		cl: TestClassArrayNestedForcedUndef,
		data: {a: undefined},
		expect: `{"a":[]}`
	},
	{
		name: 'hydrate from db',
		cl: TestClassFromDatabase,
		data: d4,
		fromDb: true,
		expect: `{"a":"value from db","id":99}`
	},
	{
		name: 'hydrate custom',
		cl: TestClassCustom,
		data: d5,
		expect: `{"cust":"_asd"}`
	},
	{
		name: 'hydrate translate',
		cl: TestClassTranslate,
		data: d6,
		expect: `{"one":"value from plain but translated"}`
	},
	{
		name: 'hydrate injection',
		cl: TestClassInjection,
		data: d7,
		expect: `{"b":7}`
	},
	{
		name: 'hydrate ignore',
		cl: TestClassIgnore,
		data: d8,
		expect: `{"a":9,"b":7}`
	}
];

for (const test of tests) {
	Test.run(test.name, () => {
		const out = Hydrator.hydrate(test.cl as any, test.data, test.fromDb);
		console.log(out);
		const string = stringify(out);
		if (string !== test.expect) {
			return string;
		}
		return true;
	});
}

const v = Hydrator.hydrateArray(TestClass, null);
console.log(v);