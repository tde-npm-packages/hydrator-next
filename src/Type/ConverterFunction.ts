import { DataDirection } from '../DataDirection';

export type ConverterFunction = (value: any, dataDirection: DataDirection) => any