import { ConverterFunction } from './Type/ConverterFunction';
import { DataConverterInfo } from './DataConverterInfo';
import { DataDirection }     from './DataDirection';

export class Converter {
	static converters = new Map<string, ConverterFunction>();

	/**
	 * Registers a converter for a specific type. Does nothing if the converter is already registered.
	 *
	 * @param type The type to register the converter for.
	 * @param converter The converter to register.
	 */
	static register(type: string, converter: ConverterFunction) {
		if (Converter.converters.has(type)) {
			return;
		}

		Converter.converters.set(type, converter);
	}

	/**
	 * Registers a converter for a specific type. Throws an error if the converter is already registered.
	 *
	 * @param type The type to register the converter for.
	 * @param converter The converter to register.
	 */
	static registerConverter(type: string, converter: ConverterFunction) {
		if (Converter.converters.has(type)) {
			throw new Error(`Converter of type: ${ type } is already registered`);
		}

		Converter.converters.set(type, converter);
	}

	/**
	 * Converts a value from one type to another including arrays.
	 */
	static convert(value: any, info: DataConverterInfo, direction: DataDirection) {
		if (info.array) {
			if (!info.forceArray) {
				if (value === null) {
					return null;
				}

				if (value === undefined) {
					return undefined;
				}
			}

			const out = [];
			for (const val of value) {
				out.push(Converter.convertSingle(val, info, direction));
			}

			value = out;
		}
		else {
			value = Converter.convertSingle(value, info, direction);
		}

		return value;
	}

	/**
	 * Converts a single value from one type to another.
	 */
	static convertSingle(value: any, info: DataConverterInfo, direction: DataDirection) {
		if (typeof value === 'undefined') {
			return undefined;
		}

		if (info.converter) {
			return info.converter(value, direction, info.type);
		}

		if (typeof info.type === 'undefined') {
			return value;
		}

		if (!Converter.converters.has(info.type)) {
			throw new Error(`Unrecognized converter type: ${ info.type }`);
		}

		return Converter.converters.get(info.type)!(value, direction);
	}
}