export enum DataDirection {
	ToPlain = 1,
	ToDatabase = 2,
	FromDatabase = 3,
	FromPlain = 4,
}